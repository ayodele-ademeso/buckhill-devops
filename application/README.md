# Part 2 - Application 

## Introduction 

Concerning the application side, the client wants to deploy a WordPress application behind an AWS Load Balancer.

To satisfy the application requirements, we need to update the IAM user permissions to be able to pull/push the containerized application into ECR, to integrate EKS cluster with Gitlab , and to verify the deployment files.

## Tasks

1. Integrate EKS cluster with Gitlab using Gitlab agent

2. Attach the appropriate IAM ECR user permissions using Terragrunt

3. Check and adapt the configuration files (.gitlab-ci.yml and K8S files) 

4. Add AWS region, ECR URL and AWS credentials (ECR IAM user) as project GitLab variables in order to run the WordPress application

5. Add the appropriate record to the Route53 using AWS console to run the application on **wordpress.devops.exam.buckhill.co.uk**

6. Screenshot the working **wordpress.devops.exam.buckhill.co.uk** URL or the LB URL and upload the image in this project (PNG)

## Notes

- To integrate EKS cluster with GitLab, follow these steps : 

    1.	Go to the [Application](https://git.buckhill.co.uk/terragrunt/devops-exam/ayodele/application) Gitlab project  
    2.  From the left sidebar, select **Infrastructure > Kubernetes** clusters.
    3.	Select Connect a cluster (agent)
    4.  Select devops-exam agent and proceed
    5.  GitLab generates an access token for the agent
    6.  Copy the command under Recommended installation method
    7.  In your terminal, Ensure that you’re conntected to the desired K8S cluster 
           
            aws eks update-kubeconfig --name <cluster_name> --region <aws_region>

    8.  Run the Gitlab commands
    9.  Once the gitlab-kubernetes-agent deployment in up and running, the agent go from disconnected to connected status
