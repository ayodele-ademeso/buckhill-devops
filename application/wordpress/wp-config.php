<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */


// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '${WORDPRESS_DB_NAME}' );

/** Database username */
define( 'DB_USER', '${WORDPRESS_DB_USER}');

/** Database password */
define( 'DB_PASSWORD', '${WORDPRESS_DB_PASSWORD}' );

/** Database hostname */
define( 'DB_HOST', '${WORDPRESS_DB_HOST}' );

define ('WP_DEBUG', true);

/** Database Table Prefix */
$table_prefix = 'wp_';

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';