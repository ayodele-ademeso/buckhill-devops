# Part 3 - Database

## Introduction 

After checking that the application is running correctly and accessible through **wordpress.devops.exam.buckhill.co.uk**, the client wants to create a new DB user with the following specifications : 

- Username : devopsexam

- Password : examDevOps!+

- Permissions : grant **SELECT** permission on **TableDevopsExam** for **devopsexam** user 

## Tasks

1. Connect to the database using any method that you want

2. Create the appropriate structures in the database


Before you finish, check the commits you made to your working repository and make sure to destroy all the environment using this command : 

    terragrunt run-all destroy

## Notes

 - Add your SQL statements here :

         # SQL Answers
         # CREATE USER 'devopsexam'@'ayodele-devops-test-master.c0heuyhd00vh.eu-west-2.rds.amazonaws.com' IDENTIFIED WITH authentication_plugin BY 'examDevOps!+';
         # GRANT SELECT ON TableDevopsExam TO 'devopsexam'@'ayodele-devops-test-master.c0heuyhd00vh.eu-west-2.rds.amazonaws.com';
         #
         #
         #
         #
         #

 - Destroy all non-Terraform resources before running terragrunt run-all destroy

 - Destroy the Terraform state S3 bucket 


