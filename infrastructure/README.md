# Part 1 - Infrastructure  

## Introduction 

To provide our clients with the appropriate cloud infrastructure and microservice applications, we use :

- Terragrunt  and Terraform as IaC tools

- Kubernetes (EKS) as container orchestrator

- Gitlab CI/CD pipelines to deploy microservice applications

The following diagram represents a cloud architecture for a Buckhill client.

The exam is divided into 3 parts.

<div align="center">
<img src="https://i.ibb.co/CMybr9c/devops-test-level-3-drawio.png"/>
</div>

| Terragrunt Module | Resources created                                                                                                                              | 
|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| IAM               | <ul><li>1 IAM User and 1 IAM group with customized IAM policies</li></ul>     | 
| VPC               | <ul><li>1 VPC</li><li>3 Public subnets and 3 private subnets distributed in 3 Availability zones</li><li>NAT GW with EIP and IGW</li><li>1 Routing table for public subnets pointing to the IGW</li><li>1 Routing table for private subnets pointing the NAT GW</li></ul> | 
| RDS               | <ul><li>1 Master DB</li><li>Cloud watch log groups for RDS</li><li>1 DB Subnet group (public subnets)</li><li>1 DB option group</li><li>1 DB parameter group</li><li>1 Security group</li><li>1 IAM role for RDS monitoring</li></ul> | 
| EKS               | <ul><li>EKS cluster</li><li>Worker nodes ( 1 EC2 instance ) attached to an autoscaling group with launch template</li><li>3 security groups for managing Cluster/Nodes communications</li><li>2 IAM roles for EKS and worker nodes</li><li>1 CloudWatch log group to log controle plane events</li><li>Kubeconfig file named <cluster_name> generated in <config_output_path> variable</li></ul> |
| ECR               | <ul><li>1 ECR private repository with life-cycle policy</li></ul> |
| EFS               | <ul><li>EFS file system</li><li>3 mount targets (3 private subnets)</li><li>1 security group</li></ul> |

**Note** :
 
 * The Exam AWS account initially contains a **KMS key** and a **Route53 hosted zone** needed for the exam
  
## Tasks

- Deploy all the above infrastructure this using this command ( it will take about **15-20** minutes) : 

        terragrunt run-all apply 
        
1. Find the typographical error blocking RDS access and adjust it in Terraform code
2. Check Terraform code for VPC and fix the networking traffic issue
3. Update the Terraform output in order to get the RDS endpoint
5. Install AWS EFS required package, and mount EFS filesystem for all EKS worker nodes using FS ID on **/mnt/efs**


## Notes

- Use SSM agent to connect the EC2 instance

- Ignore Terraform/Terragrunt Backend warnings

- Find RDS credentials in this sops-kms-encrypted file under Terragrunt regional-modules using this command : 
        
        sops env_vars.yaml 

- Use SSM CLI to connect to the EC2 instances and paste here the steps followed to intall packages, mount EFS, and verify mount status

         # EFS Answers
         # aws ssm start-session --target <instance-id>
         # sudo yum install -y amazon-efs-utils
         # sudo mkdir /mnt/efs
         # sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-09f769e997ceb5e57.efs.us-east-1.amazonaws.com:/ /mnt/efs
         # cd /mnt/efs && touch test.txt
         



