terraform {
  source = "../../../aws-terraform-modules//eks"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name-vpc                      = "${local.variables.client-name}-devops-${local.env.prefix}"
  cluster_name                  = "${local.variables.client-name}-devops-${local.env.prefix}"
  cluster_version               = "1.23"
  config_output_path            = "~/.kube/"
  encrypt_secrets               = true 
  kms_key_alias                 = "buckhill-devops-global"
  
  workers_additional_policies   = ["arn:aws:iam::aws:policy/AmazonSSMFullAccess", "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore", "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"]

  worker_groups_launch_template = [
    {
      name                 = "${local.variables.client-name}-devops-${local.env.prefix}-1"
      instance_type        = "t3.medium"
      public_ip            = false
      asg_min_size         = 2
      asg_desired_capacity = 2
      asg_max_size         = 2
      additional_userdata  = file("./additional_userdata.sh")
    }
  ]
  
}

locals {
  variables = yamldecode(file("${find_in_parent_folders("global_vars.yaml")}"))
  env       = yamldecode(sops_decrypt_file("${find_in_parent_folders("env_vars.yaml")}"))
}

dependencies {
  paths = ["../vpc"]
}
