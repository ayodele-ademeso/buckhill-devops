terraform {
  source = "../../../aws-terraform-modules//ecr"
}

include {
  path = find_in_parent_folders()
}

inputs = {

  repositories = [

    { name                    = "devops-exam"
      max_image_count         = 500
      enable_lifecycle_policy = true

      tags = {
        Name = "devops-exam"
      }
    }

  ]
}

locals {
  variables = yamldecode(file("${find_in_parent_folders("global_vars.yaml")}"))
  env       = yamldecode(sops_decrypt_file("${find_in_parent_folders("env_vars.yaml")}"))
}
