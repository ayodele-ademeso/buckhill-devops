
terraform {
  source = "../../../aws-terraform-modules//vpc"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name                              = "${local.variables.client-name}-devops-${local.env.prefix}"
  azs                               = "${local.variables.availability-zones}"
  cidr                              = "10.1.0.0/16"
  private_subnets                   = ["10.1.4.0/24", "10.1.5.0/24", "10.1.6.0/24"]
  public_subnets                    = ["10.1.40.0/24", "10.1.50.0/24", "10.1.60.0/24"]
  enable_dns_hostnames              = true
  enable_dns_support                = true
  one_nat_gateway_per_az            = false
  single_nat_gateway                = true
  enable_nat_gateway                = true
  kms_key_alias                     = "buckhill-devops-global"
  flow_log_cloudwatch_iam_role_name = "${local.variables.client-name}-devops-${local.env.prefix}"

  # Necessary tags for VPC subnets used by Amazon EKS cluster for automatic subnet discovery
  # In this example the cluster name is "${local.variables.client-name}-${local.env.prefix}"
  # It must be the same name used in eks terragrunt.hcl
  
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.variables.client-name}-devops-${local.env.prefix}" = "shared"
    "kubernetes.io/role/elb"                                                          = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${local.variables.client-name}-devops-${local.env.prefix}" = "shared"
    "kubernetes.io/role/internal-elb"                                                 = "1"
  }
}

locals {
  variables = yamldecode(file("${find_in_parent_folders("global_vars.yaml")}"))
  env       = yamldecode(sops_decrypt_file(find_in_parent_folders("env_vars.yaml")))
}

