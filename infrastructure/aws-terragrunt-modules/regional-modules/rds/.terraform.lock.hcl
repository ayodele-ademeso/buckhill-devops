# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.42.0"
  constraints = ">= 2.49.0, >= 3.0.0, >= 3.20.0"
  hashes = [
    "h1:C6/yDp6BhuDFx0qdkBuJj/OWUJpAoraHTJaU6ac38Rw=",
    "h1:quV6hK7ewiHWBznGWCb/gJ6JAPm6UtouBUrhAjv6oRY=",
    "zh:126c856a6eedddd8571f161a826a407ba5655a37a6241393560a96b8c4beca1a",
    "zh:1a4868e6ac734b5fc2e79a4a889d176286b66664aad709435aa6acee5871d5b0",
    "zh:40fed7637ab8ddeb93bef06aded35d970f0628025b97459ae805463e8aa0a58a",
    "zh:68def3c0a5a1aac1db6372c51daef858b707f03052626d3427ac24cba6f2014d",
    "zh:6db7ec9c8d1803a0b6f40a664aa892e0f8894562de83061fa7ac1bc51ff5e7e5",
    "zh:7058abaad595930b3f97dc04e45c112b2dbf37d098372a849081f7081da2fb52",
    "zh:8c25adb15a19da301c478aa1f4a4d8647cabdf8e5dae8331d4490f80ea718c26",
    "zh:8e129b847401e39fcbc54817726dab877f36b7f00ff5ed76f7b43470abe99ff9",
    "zh:d268bb267a2d6b39df7ddee8efa7c1ef7a15cf335dfa5f2e64c9dae9b623a1b8",
    "zh:d6eeb3614a0ab50f8e9ab5666ae5754ea668ce327310e5b21b7f04a18d7611a8",
    "zh:f5d3c58055dff6e38562b75d3edc908cb2f1e45c6914f6b00f4773359ce49324",
  ]
}

provider "registry.terraform.io/terraform-providers/mysql" {
  version     = "1.9.0"
  constraints = ">= 1.5.0"
  hashes = [
    "h1:qC9l8/oK2HcOcSqc3QnHpWjE6U5wfZq4trk37TYjdco=",
    "h1:xCQLVKy18oE/Wo3IkK82GzRX+sEioMyPFUlRt28USiA=",
    "zh:2dbd3d607e99ef16d0e177cccf2dc570e165d8b2d6a2902db7c62ce831948301",
    "zh:34bf5a491cb015f513305b8c15e75c64d4bcde164f7f061140899a1bb5042d37",
    "zh:4814e797ebd54598ce20444951793a1d4a78c2260335f0e229441d276823664d",
    "zh:4b251d1b9c5dbf9176443dd79b98e3f25b44f602c9775cf0c06dafc7049007b2",
    "zh:58011051a7ec9274225d92876aff2967b8469189682f22f528f2b152a5da8aba",
    "zh:8c92b32ca9485806ec45e1bb8f2e0e724db9fd5829af8f9d6684df05e443bd16",
    "zh:91fa4e53d56e0d47265ebe84addb1dfc15da0645e7b1730372178d35f8d1c57d",
    "zh:bcae78fe8b1527064f0c5ef4ccc9c26daef11fe8a812da661cb34b1b863e79fd",
    "zh:c9bb7d9638bbd764335d744d9d40bd08b2dd661d136e02825f21261aa5616d76",
    "zh:ccd11453a13ef3af8ae136a1470d13badf31093afc3fc318e92f7f48ef8d65a2",
    "zh:d6a3e7a2829c66d018271a565cae46fe959e48cb481d478c155fe38790724dda",
    "zh:fc5e948669a6a33c1b98aa89466294aa97983b00abed1db6efe568a128d629ae",
  ]
}
