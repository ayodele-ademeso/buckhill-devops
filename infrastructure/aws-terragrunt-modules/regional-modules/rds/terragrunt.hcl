terraform {
  source = "../../../aws-terraform-modules//rds"
}

include {
  path = find_in_parent_folders()
}

inputs = {

  # DB instance
  name-vpc                              = "${local.variables.client-name}-devops-${local.env.prefix}"
  identifier                            = "${local.variables.client-name}-devops-${local.env.prefix}"
  use_private_subnets                   = false 
  instance_class                        = "db.t2.small"
  allocated_storage                     = 10
  max_allocated_storage                 = 15
  storage_type                          = "gp2" 
  engine                                = "mariadb"
  engine_version                        = "10.6.7"
  multi_az                              = false
  kms_key_alias                         = "buckhill-devops-global"
  create_monitoring_role                = true
  monitoring_role_name                  = "${local.variables.client-name}-devops-${local.env.prefix}-rds-monitoring-role"

  publicly_accessible             = true
  storage_encrypted               = true
  enabled_cloudwatch_logs_exports = ["error", "general", "slowquery", "audit"]
  deletion_protection             = false

  # DB parameter group
  parameter_group_name = "${local.variables.client-name}-devops-${local.env.prefix}"
  family               = "mariadb10.6"
  
  # DB option group
  option_group_name    = "${local.variables.client-name}-devops-${local.env.prefix}"
  major_engine_version = "10.6"

  # Master DB credentials 
  name_db            = "${local.env.name_db}"
  username           = "${local.env.username_db}"
  password           = "${local.env.password_db}"
  external_access_db = "${local.env.external_access}"

}

locals {
  variables = yamldecode(file("${find_in_parent_folders("global_vars.yaml")}"))
  env       = yamldecode(sops_decrypt_file("${find_in_parent_folders("env_vars.yaml")}"))
}

dependencies {
  paths = ["../vpc"]
}

