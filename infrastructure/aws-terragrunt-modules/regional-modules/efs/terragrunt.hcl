terraform {
  source = "../../../aws-terraform-modules//efs"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name-vpc            = "${local.variables.client-name}-devops-${local.env.prefix}"
  name                = "${local.variables.client-name}-devops-${local.env.prefix}"
  region              = "${local.variables.region}"
  availability_zones  = "${local.variables.availability-zones}"
  use_private_subnets = true
  encrypted           = true
  allowed_cidr_blocks = ["10.1.0.0/16"]

} 


locals {
  variables = yamldecode(file("${find_in_parent_folders("global_vars.yaml")}"))
  env       = yamldecode(sops_decrypt_file("${find_in_parent_folders("env_vars.yaml")}"))
}

dependencies {
  paths = ["../vpc"]
}