
terraform {
  source = "../../../aws-terraform-modules//iam"
}

include {
  path = find_in_parent_folders()
}

inputs = {

  create_account_password_policy           = false

  users = [
    {
      name                          = "ecr-devops-exam"
      create_iam_user_login_profile = false
      create_iam_access_key         = true
      credentials_file_output_path  = "~/.credentials/"
      tags                          =  {} 
    },
  ]

  groups = [
    {
      name        = "ecr-devops-exam"
      group_users = ["ecr-devops-exam"]
      custom_group_policies = [
        {
          name   = "ecr-devops-exam"
          policy = file("policy.json")
        },
      ]
    },
  ]
}

locals {
  variables = yamldecode(file("${find_in_parent_folders("global_vars.yaml")}"))
}
