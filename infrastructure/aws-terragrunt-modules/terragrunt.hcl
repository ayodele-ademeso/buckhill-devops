remote_state {
  backend = "s3"
  config = {
    encrypt = true
    bucket  = "${local.variables.s3-tfstate-bucket-name}"
    key     = "${path_relative_to_include()}/terraform.tfstate"
    region  = "${local.variables.region}"
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite"
  contents  = <<EOF
  provider "aws" {
    region              = "${local.variables.region}"
  }
  EOF
}

locals {
  variables = yamldecode(file("global_vars.yaml"))
}

####### Tags for all resources #########
inputs = (
  {
    tags = {
      Owner = "Buckhill"
      Candidate = "ayodele"
    }
  }
)

