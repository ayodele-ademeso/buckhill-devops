terraform {
  backend "s3" {}
}

module "repos" {
  source = "./modules/repository"
  count  = length(var.repositories)

  name                            = lookup(var.repositories[count.index], "name", count.index)
  scan_images_on_push             = lookup(var.repositories[count.index], "scan_images_on_push", true)
  max_image_count                 = lookup(var.repositories[count.index], "max_image_count", 500)
  image_tag_mutability            = lookup(var.repositories[count.index], "image_tag_mutability", "IMMUTABLE")
  enable_lifecycle_policy         = lookup(var.repositories[count.index], "enable_lifecycle_policy", true)
  attach_untagged_images_rule     = lookup(var.repositories[count.index], "attach_untagged_images_rule", true)
  rotate_images_rule_description  = lookup(var.repositories[count.index], "rotate_images_rule_description", "")
  untagged_image_rule_description = lookup(var.repositories[count.index], "untagged_image_rule_description", "" )
  attach_rotate_images_rule       = lookup(var.repositories[count.index], "attach_rotate_images_rule", true)
  untagged_rule_count             = lookup(var.repositories[count.index], "untagged_rule_count", 0 )
  encryption_configuration        = lookup(var.repositories[count.index], "encryption_configuration", null)
  principals_full_access          = lookup(var.repositories[count.index], "principals_full_access", [])
  principals_readonly_access      = lookup(var.repositories[count.index], "principals_readonly_access", [])
  protected_tags                  = lookup(var.repositories[count.index], "protected_tags", [])
  tags                            = merge(var.tags,lookup(var.repositories[count.index], "tags", {}))
}

