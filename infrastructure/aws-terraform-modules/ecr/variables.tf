variable "repositories" {
  description = "A list of maps defining iam users"
  type        = any
  default     = []
}


variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags"
}