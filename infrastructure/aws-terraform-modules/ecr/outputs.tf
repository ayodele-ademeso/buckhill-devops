output "repositories-created" {
  value       = module.repos[*].name
  description = "Name of repositories created"
}
