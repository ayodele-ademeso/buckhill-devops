variable "identifier" {
  description = "The name of the RDS instance, if omitted, Terraform will assign a random, unique identifier"
  type        = string
  default     = ""
}

variable "allocated_storage" {
  description = "The allocated storage in gigabytes"
  type        = string
}

variable "allocated_storage_replica" {
  description = "The allocated storage in gigabytes for replica db instance"
  type        = string
  default     = ""
}

variable "sg_ingress_rules" {
  description = "List of ingress rules to create where 'cidr_blocks' is used"
  type        = list(map(string))
  default     = null
}

variable "storage_type" {
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD). The default is 'io1' if iops is specified, 'gp2' if not."
  type        = string
  default     = "gp2"
}


variable "subnet_group_description" {
  description = "The description for db subnet group"
  type        = string
  default     = ""
}


variable "db_subnet_name_tag_value" {
  description = "A tag value for db subnet group Name tag key"
  type        = string
  default     = ""
} 

variable "security_group_description" {
  description = "The description for db security group"
  type        = string
  default     = ""
}

variable "additional_db_sg_ingress_rules" {
  description = "additional ingress rules besides VPC and external public IP"
  type        = any
  default     = []
}

variable "create_standalone_security_group_read_replica" {
  description = "Create standalone SG for read replica or not (attach SG of master DB to read replica)"
  type        = bool
  default     = false
}

variable "security_group_name_read_replica" {
  description = "The name for db security group for read replica"
  type        = string
  default     = ""
}

variable "security_group_description_read_replica" {
  description = "The description for db security group"
  type        = string
  default     = ""
}

variable "security_group_name" {
  description = "The name for db security group"
  type        = string
  default     = ""
}

variable "enable_sns" {
  type        = bool
  default     = true
  description = "Whether to enable SNS resources"
}

variable "use_private_subnets" {
  type        = bool
  description = "Wether subnet group for DB instance is in private subnets or not (public subnets)"
  default     = true
}

variable "create_rds_os_metrics_log_group" {
  type        = bool
  default     = false
  description = "Whether to create RDSOSMetrics cloudwatch log group"
}

variable "storage_encrypted" {
  description = "Specifies whether the DB instance is encrypted"
  type        = bool
  default     = false
}

variable "storage_encrypted_replica" {
  description = "Specifies whether the replica DB instance is encrypted"
  type        = bool
  default     = null
}


variable "kms_key_id" {
  description = "The ARN for the KMS encryption key. If creating an encrypted replica, set this to the destination KMS ARN. If storage_encrypted is set to true and kms_key_id is not specified the default KMS key created in your account will be used"
  type        = string
  default     = ""
}

variable "replicate_source_db" {
  description = "Specifies that this resource is a Replicate database, and to use this value as the source database. This correlates to the identifier of another Amazon RDS Database to replicate."
  type        = string
  default     = null
}

variable "snapshot_identifier" {
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05."
  type        = string
  default     = null
}

variable "license_model" {
  description = "License model information for this DB instance. Optional, but required for some DB engines, i.e. Oracle SE1"
  type        = string
  default     = ""
}

variable "iam_database_authentication_enabled" {
  description = "Specifies whether or not the mappings of AWS Identity and Access Management (IAM) accounts to database accounts are enabled"
  type        = bool
  default     = false
}

variable "domain" {
  description = "The ID of the Directory Service Active Directory domain to create the instance in"
  type        = string
  default     = ""
}

variable "domain_iam_role_name" {
  description = "(Required if domain is provided) The name of the IAM role to be used when making API calls to the Directory Service"
  type        = string
  default     = ""
}

variable "engine" {
  description = "The database engine to use"
  type        = string
}

variable "engine_version" {
  description = "The engine version to use"
  type        = string
}

variable "engine_replica" {
  description = "The replica database engine to use"
  type        = string
  default     = ""
}

variable "engine_version_replica" {
  description = "The engine version to use for replica"
  type        = string
  default     = ""
}

variable "final_snapshot_identifier" {
  description = "The name of your final DB snapshot when this DB instance is deleted."
  type        = string
  default     = null
}

variable "instance_class" {
  description = "The instance type of the RDS instance"
  type        = string
}

variable "instance_class_replica" {
  description = "The instance type of the RDS replica instance"
  type        = string
  default     = ""
}

variable "name_db" {
  description = "The DB name to create. If omitted, no database is created initially"
  type        = string
  default     = ""
}

variable "name-vpc" {
  description = "Name of the vpc to find"
  type        = string
  default     = ""
}

variable "username" {
  description = "Username for the master DB user"
  type        = string
}

variable "password" {
  description = "Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file"
  type        = string
}

variable "port" {
  description = "The port on which the DB accepts connections"
  type        = number
  default     = 3306
}

variable "vpc_security_group_ids" {
  description = "List of VPC security groups to associate"
  type        = list(string)
  default     = []
}

variable "db_subnet_group_name" {
  description = "Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group. If unspecified, will be created in the default VPC"
  type        = string
  default     = ""
}

variable "database_subnet_group_name" {
  description = "Name of DB subnet group"
  type        = string
  default     = ""
}

variable "replica_identifier" {
  description = "identifier of the read-replica"
  type        = string
  default     = ""
}

variable "master_db_identifier" {
  description = "identifier of the master db instance"
  type        = string
  default     = ""
}



variable "availability_zone" {
  description = "The Availability Zone of the RDS instance"
  type        = string
  default     = ""
}

variable "multi_az_replica" {
  description = "Specifies if the RDS replica instance is multi-AZ"
  type        = bool
  default     = null
}


variable "multi_az" {
  description = "Specifies if the RDS instance is multi-AZ"
  type        = bool
  default     = false
}

variable "iops" {
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of 'io1'"
  type        = number
  default     = 0
}

variable "publicly_accessible" {
  description = "Bool to control if instance is publicly accessible"
  type        = bool
  default     = false
}

variable "publicly_accessible_replica" {
  description = "Bool to control if replica instance is publicly accessible"
  type        = bool
  default     = null
}

variable "monitoring_interval" {
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60."
  type        = number
  default     = 0
}

variable "monitoring_interval_replica" {
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the replica DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60."
  type        = number
  default     = null
}

variable "monitoring_role_arn" {
  description = "The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs. Must be specified if monitoring_interval is non-zero."
  type        = string
  default     = ""
}

variable "monitoring_role_name" {
  description = "Name of the IAM role which will be created when create_monitoring_role is enabled."
  type        = string
  default     = "rds-monitoring-role"
}

variable "create_monitoring_role" {
  description = "Create IAM role with a defined name that permits RDS to send enhanced monitoring metrics to CloudWatch Logs."
  type        = bool
  default     = false
}

variable "allow_major_version_upgrade" {
  description = "Indicates that major version upgrades are allowed. Changing this parameter does not result in an outage and the change is asynchronously applied as soon as possible"
  type        = bool
  default     = false
}

variable "auto_minor_version_upgrade" {
  description = "Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window"
  type        = bool
  default     = true
}

variable "apply_immediately" {
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
  type        = bool
  default     = false
}

variable "maintenance_window" {
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00'"
  type        = string
  default     = "Mon:00:00-Mon:03:00"
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final_snapshot_identifier"
  type        = bool
  default     = true
}


variable "skip_final_snapshot_replica" {
  description = "Determines whether a final DB snapshot is created before the replica DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final_snapshot_identifier"
  type        = bool
  default     = null
}

variable "copy_tags_to_snapshot" {
  description = "On delete, copy all Instance tags to the final snapshot (if final_snapshot_identifier is specified)"
  type        = bool
  default     = false
}

variable "backup_retention_period" {
  description = "The days to retain backups for"
  type        = number
  default     = 30
}

variable "backup_retention_period_replica" {
  description = "Replica DB : The days to retain backups for"
  type        = number
  default     = null
}

variable "backup_window" {
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled. Example: '09:46-10:16'. Must not overlap with maintenance_window"
  type        = string
  default     = "03:00-06:00"
}

variable "s3_import" {
  description = "Restore from a Percona Xtrabackup in S3 (only MySQL is supported)"
  type        = map(string)
  default     = null
}

variable "tags" {
  description = "A mapping of tags to assign to all resources"
  type        = map(string)
  default     = {}
}

variable "subnet_group_tags" {
  description = "A mapping of tags to assign to subnet group"
  type        = map(string)
  default     = {}
}

variable "parameter_group_tags" {
  description = "A mapping of tags to assign to parameter group"
  type        = map(string)
  default     = {}
}

variable "option_group_tags" {
  description = "A mapping of tags to assign to option group"
  type        = map(string)
  default     = {}
}



variable "db_subnet_group_tags" {
  description = "A mapping of tags to assign DB subnet group"
  type        = map(string)
  default     = {}
}

# DB subnet group
variable "subnet_ids" {
  description = "A list of VPC subnet IDs"
  type        = list(string)
  default     = []
}

# DB parameter group
variable "create_db_parameter_group" {
  description = "Whether to create a database parameter group"
  type        = bool
  default     = true
}

variable "parameter_group_name" {
  description = "Name of the DB parameter group to associate or create"
  type        = string
  default     = ""
}

variable "parameter_group_use_name_prefix" {
  description = "Determines whether to use `parameter_group_name` as is or create a unique name beginning with the `parameter_group_name` as the prefix"
  type        = bool
  default     = false
}

variable "parameter_group_description" {
  description = "Description of the DB parameter group to create"
  type        = string
  default     = ""
}

variable "family" {
  description = "The family of the DB parameter group"
  type        = string
  default     = ""
}

variable "parameters" {
  description = "A list of DB parameters (map) to apply"
  type        = list(map(string))
  default     = []
}

# DB option group
variable "create_db_option_group" {
  description = "(Optional) Create a database option group"
  type        = bool
  default     = true
}

variable "option_group_name" {
  description = "Name of the option group"
  type        = string
  default     = ""
}

variable "option_group_use_name_prefix" {
  description = "Determines whether to use `option_group_name` as is or create a unique name beginning with the `option_group_name` as the prefix"
  type        = bool
  default     = false
}

variable "option_group_description" {
  description = "The description of the option group"
  type        = string
  default     = ""
}

variable "major_engine_version" {
  description = "Specifies the major version of the engine that this option group should be associated with"
  type        = string
  default     = ""
}

variable "options" {
  description = "A list of Options to apply."
  type        = any
  default     = []
}

variable "create_db_subnet_group" {
  description = "Whether to create a database subnet group"
  type        = bool
  default     = true
}

variable "create_db_instance" {
  description = "Whether to create a database instance"
  type        = bool
  default     = true
}

variable "timezone" {
  description = "(Optional) Time zone of the DB instance. timezone is currently only supported by Microsoft SQL Server. The timezone can only be set on creation. See MSSQL User Guide for more information."
  type        = string
  default     = ""
}

variable "character_set_name" {
  description = "(Optional) The character set name to use for DB encoding in Oracle instances. This can't be changed. See Oracle Character Sets Supported in Amazon RDS for more information"
  type        = string
  default     = ""
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL), upgrade (PostgreSQL)."
  type        = list(string)
  default     = []
}

variable "log_retention_in_days" {
  default     = 365
  description = "Number of days to retain log events. Default retention - 365 days."
  type        = number
}

variable "log_retention_in_days_os_metrics" {
  default     = 365
  description = "Number of days to retain log events for OS Metrics RDS. Default retention - 365 days."
  type        = number
}


variable "kms_key_alias" {
  description = "the alias of the kms key to encrypt findings in s3 and to encrypt also sns topic and cloudwatch log groups"
  type        = string
  default     = ""
}

variable "timeouts" {
  description = "(Optional) Updated Terraform resource management timeouts. Applies to `aws_db_instance` in particular to permit resource management times"
  type        = map(string)
  default = {
    create = "40m"
    update = "80m"
    delete = "40m"
  }
}

variable "option_group_timeouts" {
  description = "Define maximum timeout for deletion of `aws_db_option_group` resource"
  type        = map(string)
  default = {
    delete = "15m"
  }
}

variable "deletion_protection" {
  description = "The database can't be deleted when this value is set to true."
  type        = bool
  default     = false
}

variable "deletion_protection_replica" {
  description = "The replica database can't be deleted when this value is set to true."
  type        = bool
  default     = null
}

variable "performance_insights_enabled" {
  description = "Specifies whether Performance Insights are enabled"
  type        = bool
  default     = false
}

variable "performance_insights_retention_period" {
  description = "The amount of time in days to retain Performance Insights data. Either 7 (7 days) or 731 (2 years)."
  type        = number
  default     = 0
}

variable "replica_performance_insights_enabled" {
  description = "replica instance : Specifies whether Performance Insights are enabled"
  type        = bool
  default     = false
}

variable "replica_performance_insights_retention_period" {
  description = "replica instance : The amount of time in days to retain Performance Insights data. Either 7 (7 days) or 731 (2 years)."
  type        = number
  default     = 7
}

variable "encrypt_cloudwatch_log_group" {
  description = "Wether to encrypt CW log groups for DB instance or not"
  type        = bool
  default     = true
}


variable "performance_insights_kms_key_alias" {
  description = "The alias for the KMS key to encrypt Performance Insights data."
  type        = string
  default     = "aws/rds"
}

variable "max_allocated_storage" {
  description = "Specifies the value for Storage Autoscaling"
  type        = number
  default     = 0
}

variable "max_allocated_storage_replica" {
  description = "Specifies the value for Storage Autoscaling for db replica instance"
  type        = number
  default     = null
}

variable "ca_cert_identifier" {
  description = "Specifies the identifier of the CA certificate for the DB instance"
  type        = string
  default     = "rds-ca-2019"
}

variable "delete_automated_backups" {
  description = "Specifies whether to remove automated backups immediately after the DB instance is deleted"
  type        = bool
  default     = true
}

variable "create_read_replica" {
  description = "Wether to create a read replica or not"
  type        = bool
  default     = false
}

variable "external_access_db" {
  type = string
  description = "Public IP for external access db"
  default = ""
}

variable "databases" {
  description = "name of DB to create"
  type        = list(string)
  default     = []
}

variable "users" {
    description = "users to create in db"
    type=list(map(string))
    default = []
}


variable "priviliges" {
    description = "priviliges for in db"
    type=list(map(string))
    default = []
}

variable "amount_read_replica" {
  description = "Number of read replica to create"
  type        = number
  default     = 1
}

variable "name-backup" {
  description = "Name to be used for AWS bakcup"
  type        = string
  default     = ""
}

variable "emails-rds-backup" {
  description = "list of emails to subscribe to RDS backup notification events"
  type        = list(string)
  default     = []
}

variable "enable_aws_backup" {
  description = "Wether to enable AWS Backup"
  type        = bool
  default     = false
}

variable "notification-backup" {
  description = "list of aws backup notifications"
  type        = list(string)
  default     = []
}

variable "schedule" {
  description = "schedule cron expression for aws backup"
  type        = string
  default     = ""
}

variable "start_window" {
  description = "start_window for aws backup"
  type        = number
  default     = 10
}

variable "completion_window" {
  description = "completion_window for aws backup"
  type        = number
  default     = 100
}

variable "enable_continuous_backup" {
  description = "wether to enable_continuous_backup for aws backup"
  type        = bool
  default     = false
}

variable "backup_lifecycle" {
  description = "lifecycle for aws backup"
  type        = map(string)
  default     = {}
}

variable "copy_action" {
  description = "copy_action for aws backup"
  type        = map(string)
  default     = {}
}

variable "recovery_point_tags" {
  description = "recovery_point_tags for aws backup"
  type        = map(string)
  default     = {}
}
