resource "aws_db_subnet_group" "this" {
  count = var.create ? 1 : 0

  name = var.name-subnet-group
  description = var.description == "" ? "Database subnet group for ${var.identifier}" : var.description
  subnet_ids  = var.subnet_ids

  tags = merge(
    {
      "Name" = format("%s", local.db_subnet_name_tag)
    },
    var.tags,

  )
}

locals {
  db_subnet_name_tag = var.identifier == "" ? var.db_subnet_name_tag_value  : var.identifier
}