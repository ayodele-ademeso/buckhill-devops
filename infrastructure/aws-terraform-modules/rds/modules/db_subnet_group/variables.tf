variable "create" {
  description = "Whether to create this resource or not?"
  type        = bool
  default     = true
}

variable "name-subnet-group" {
  description = "name of subnet group"
  type        = string
}

variable "identifier" {
  description = "The identifier of the resource"
  type        = string
  default     = ""
}

variable "description" {
  description = "The description for db subnet group"
  type        = string
  default     = ""
}

variable "tags" {
  description = "A mapping of tags to assign DB subnet group"
  type        = map(string)
  default     = {}
}


variable "subnet_ids" {
  description = "A list of VPC subnet IDs"
  type        = list(string)
  default     = []
}

variable "db_subnet_name_tag_value" {
  description = "A tag value for db subnet group Name tag key"
  type        = string
  default     = ""
}

variable "db_subnet_group_tags" {
  description = "Additional tags for DB subnet group"
  type        = map(string)
  default     = {}
}


