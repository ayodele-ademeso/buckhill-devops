terraform {
  backend "s3" {}
}

################################################################################
# Supporting Resources
################################################################################

data "aws_kms_key" "by_alias" {
  count = var.encrypt_cloudwatch_log_group ? 1 : 0
  key_id = "alias/${var.kms_key_alias}"
}

data "aws_kms_key" "by_alias_performance_insights" {
  count = var.performance_insights_enabled == true ? 1 : 0
  key_id = "alias/${var.performance_insights_kms_key_alias}"
}

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = [var.name-vpc]
  }
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.main.id
}

data "aws_subnet_ids" "public" {
  count = var.use_private_subnets ? 0 : 1
  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = ["${var.name-vpc}-pub*"]
  }

}

data "aws_subnet_ids" "private" {
  count = var.use_private_subnets ? 1 : 0
  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = ["${var.name-vpc}-priv*"]
  }
}

locals {
  create_db_subnet_group = var.db_subnet_group_name == "" ? var.create_db_subnet_group : false
  db_subnet_group_name = coalesce(var.db_subnet_group_name, module.db_subnet_group.this_db_subnet_group_id)

  parameter_group_name_id = coalesce(var.parameter_group_name, module.db_parameter_group.this_db_parameter_group_id)

  create_db_option_group = var.create_db_option_group && var.engine != "postgres"
  option_group           = var.engine != "postgres" ? coalesce(module.db_option_group.this_db_option_group_id, var.option_group_name) : null
}

module "db_subnet_group" {
  source = "./modules/db_subnet_group"

  create      = local.create_db_subnet_group
  identifier  = var.identifier
  db_subnet_name_tag_value = var.db_subnet_name_tag_value
  description = var.subnet_group_description
  name-subnet-group  = var.database_subnet_group_name == "" ? var.identifier : var.database_subnet_group_name
  subnet_ids  = var.use_private_subnets ? tolist(data.aws_subnet_ids.private[0].ids) : tolist(data.aws_subnet_ids.public[0].ids)

  tags = merge(var.tags,var.subnet_group_tags)
}

module "db_parameter_group" {
  source = "./modules/db_parameter_group"

  create = var.create_db_parameter_group

  name            = var.parameter_group_name
  use_name_prefix = var.parameter_group_use_name_prefix
  description     = var.parameter_group_description
  family          = var.family

  parameters = var.parameters

  tags = merge(var.tags,var.parameter_group_tags)
}

module "db_option_group" {
  source = "./modules/db_option_group"

  create = local.create_db_option_group

  name                     = var.option_group_name
  use_name_prefix          = var.option_group_use_name_prefix
  option_group_description = var.option_group_description
  engine_name              = var.engine
  major_engine_version     = var.major_engine_version

  options = var.options

  timeouts = var.option_group_timeouts

  tags = merge(var.tags,var.option_group_tags)
}

module "db_instance" {
  source = "./modules/db_instance"

  create            = var.create_db_instance
  identifier        = var.master_db_identifier == "" ? "${var.identifier}-master" : var.master_db_identifier
  engine            = var.engine
  engine_version    = var.engine_version
  instance_class    = var.instance_class
  allocated_storage = var.allocated_storage
  storage_type      = var.storage_type
  storage_encrypted = var.storage_encrypted
  kms_key_id        = var.kms_key_id
  license_model     = var.license_model

  name                                = var.name_db
  username                            = var.username
  password                            = var.password
  port                                = var.port
  domain                              = var.domain
  domain_iam_role_name                = var.domain_iam_role_name
  iam_database_authentication_enabled = var.iam_database_authentication_enabled

  replicate_source_db = var.replicate_source_db

  snapshot_identifier = var.snapshot_identifier

  vpc_security_group_ids = [module.security_group.security_group_id]
  db_subnet_group_name   = local.db_subnet_group_name
  parameter_group_name   = local.parameter_group_name_id
  option_group_name      = local.option_group

  availability_zone   = var.availability_zone
  multi_az            = var.multi_az
  iops                = var.iops
  publicly_accessible = var.publicly_accessible

  ca_cert_identifier = var.ca_cert_identifier

  allow_major_version_upgrade = var.allow_major_version_upgrade
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade
  apply_immediately           = var.apply_immediately
  maintenance_window          = var.maintenance_window
  skip_final_snapshot         = var.skip_final_snapshot
  copy_tags_to_snapshot       = var.copy_tags_to_snapshot
  final_snapshot_identifier   = var.final_snapshot_identifier

  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_retention_period = var.performance_insights_retention_period
  performance_insights_kms_key_id       = var.performance_insights_enabled == true ? data.aws_kms_key.by_alias_performance_insights[0].arn : null

  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window
  max_allocated_storage   = var.max_allocated_storage
  monitoring_interval     = var.monitoring_interval
  monitoring_role_arn     = var.monitoring_role_arn
  monitoring_role_name    = var.monitoring_role_name
  create_monitoring_role  = var.create_monitoring_role

  timezone                        = var.timezone
  character_set_name              = var.character_set_name
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports

  timeouts = var.timeouts

  deletion_protection      = var.deletion_protection
  delete_automated_backups = var.delete_automated_backups

  s3_import = var.s3_import

  tags = var.tags
depends_on = [
    module.security_group,
    aws_cloudwatch_log_group.this,
    aws_cloudwatch_log_group.rds_os_metrics,
  ]
  
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3"

  name        = var.security_group_name == "" ? "${var.identifier}-db-sg" : var.security_group_name
  use_name_prefix = false
  description = var.security_group_description == "" ? "Managed by Terraform" : var.security_group_description
  vpc_id      = data.aws_vpc.main.id

  # ingress
  ingress_with_cidr_blocks = local.rules
    egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = -1
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = var.tags
}
locals {
  rules = var.sg_ingress_rules == null ? [
    {
      from_port   = var.port
      to_port     = var.port
      protocol    = "tcp"
      description = "Managed by Terraform"
      cidr_blocks = data.aws_vpc.main.cidr_block
    },

    {
      from_port   = var.port
      to_port     = var.port
      protocol    = "tcp"
      description = "Managed by Terraform"
      cidr_blocks = "${var.external_access_db}/0"
    },
  
  ] : var.sg_ingress_rules
}
resource "aws_security_group_rule" "additional_db_sg_ingress_rules" {
  count = length(var.additional_db_sg_ingress_rules)
  type                     = "ingress"
  from_port                = lookup(var.additional_db_sg_ingress_rules[count.index], "from_port", null)
  to_port                  = lookup(var.additional_db_sg_ingress_rules[count.index], "to_port", null)
  protocol                 = lookup(var.additional_db_sg_ingress_rules[count.index], "protocol",null)
  description              = lookup(var.additional_db_sg_ingress_rules[count.index], "description",null)
  cidr_blocks              = lookup(var.additional_db_sg_ingress_rules[count.index], "cidr_blocks", null)
  source_security_group_id = lookup(var.additional_db_sg_ingress_rules[count.index], "source_security_group_id", null)
  ipv6_cidr_blocks         = lookup(var.additional_db_sg_ingress_rules[count.index], "ipv6_cidr_blocks",null)
  security_group_id        = module.security_group.security_group_id
}

module "security_group_read_replica" {
  count = var.create_standalone_security_group_read_replica ? 1 : 0
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3"

  name        = var.security_group_name_read_replica == "" ? "${var.identifier}-db-sg-read-replica" : var.security_group_name_read_replica
  use_name_prefix = false
  description = var.security_group_description_read_replica == "" ? "Managed by Terraform" : var.security_group_description_read_replica 
  vpc_id      = data.aws_vpc.main.id

  # ingress
  ingress_with_cidr_blocks = [
    {
      from_port   = var.port
      to_port     = var.port
      protocol    = "tcp"
      description = "Managed by Terraform"
      cidr_blocks = data.aws_vpc.main.cidr_block
    },

    {
      from_port   = var.port
      to_port     = var.port
      protocol    = "tcp"
      description = "Managed by Terraform"
      cidr_blocks = "${var.external_access_db}/0"
    },
  
  ]
    egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = -1
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags = var.tags
}


resource "aws_db_instance" "db_instance_read_replica" {

  count       = var.create_read_replica ? var.amount_read_replica : 0

  identifier = var.replica_identifier == "" ? "${var.identifier}-replica-${count.index}" : var.replica_identifier

  # Source database. For cross-region use this_db_instance_arn
  replicate_source_db = var.master_db_identifier == "" ? "${var.identifier}-master" : var.master_db_identifier

  engine                = var.engine_replica == "" ? var.engine : var.engine_replica
  engine_version        = var.engine_version_replica == "" ? var.engine_version : var.engine_version_replica
  instance_class        = var.instance_class_replica == "" ? var.instance_class : var.instance_class_replica
  allocated_storage     = var.allocated_storage_replica == "" ? var.allocated_storage : var.allocated_storage_replica
  max_allocated_storage = var.max_allocated_storage_replica == null ? var.max_allocated_storage : var.max_allocated_storage_replica
  storage_encrypted     = var.storage_encrypted_replica == null ? var.storage_encrypted : var.storage_encrypted_replica
  skip_final_snapshot   = var.skip_final_snapshot_replica == null ? var.skip_final_snapshot : var.skip_final_snapshot_replica
  publicly_accessible   = var.publicly_accessible_replica == null ? var.publicly_accessible : var.publicly_accessible_replica
  multi_az              = var.multi_az_replica == null ? var.multi_az : var.multi_az_replica
  monitoring_interval   = var.monitoring_interval_replica == null ? var.monitoring_interval : var.monitoring_interval_replica
  monitoring_role_arn   = module.db_instance.enhanced_monitoring_iam_role_arn 

  performance_insights_enabled          = var.replica_performance_insights_enabled
  performance_insights_retention_period = var.replica_performance_insights_retention_period
  performance_insights_kms_key_id       = var.replica_performance_insights_enabled == true ? data.aws_kms_key.by_alias_performance_insights[0].arn : null
  
  # Username and password should not be set for replicas
  username = ""
  password = ""
  port     = var.port
  vpc_security_group_ids = var.create_standalone_security_group_read_replica ? [module.security_group_read_replica[0].security_group_id]: [module.security_group.security_group_id]
  maintenance_window              = var.maintenance_window
  backup_window                   = var.backup_window
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports

  backup_retention_period   = var.backup_retention_period_replica == null ? var.backup_retention_period : var.backup_retention_period_replica
  #final_snapshot_identifier = local.name
  deletion_protection       = var.deletion_protection_replica == null ? var.deletion_protection : var.deletion_protection_replica


  tags = merge(
    var.tags,
    {
      "Name" = format("%s","${var.identifier}-replica-${count.index}")
    },
  )

  depends_on = [
    module.security_group,
    #aws_cloudwatch_log_group.replica,
    aws_cloudwatch_log_group.rds_os_metrics,
  ]

  lifecycle {
    create_before_destroy = true
  }
}

# #Create db and user
# provider "mysql" {
#   endpoint = module.db_instance.this_db_instance_endpoint
#   username = var.username
#   password = var.password

# }


# resource "mysql_database" "app" {
#   count = length(var.databases)
#   name  = element(var.databases, count.index)
#   depends_on = [
#     module.db_instance,
#   ]
# }



# resource "mysql_user" "user" {
#   count = length(var.users)
#   user               = element(var.users, count.index)["username_user_db"]
#   host       = var.external_access_db
#   plaintext_password = element(var.users, count.index)["password_user_db"]

#     depends_on = [
#     mysql_database.app,
#   ]
# }


# resource "mysql_grant" "priviliges" {
#   count = length(var.priviliges)
#   user       = element(var.priviliges, count.index)["username"]
#   host       = var.external_access_db
#   database   = element(var.priviliges, count.index)["db"]
#   privileges = [element(var.priviliges, count.index)["priviliges"]]

#   depends_on = [
#     mysql_user.user,
#   ]
# }

module "rds_aws_backup" {
  count = var.enable_aws_backup ? 1 : 0
  source  = "..//aws-backup"
  name = var.name-backup
  notification  = var.notification-backup
  enable_sns = var.enable_sns
  emails = var.emails-rds-backup
  kms_key_alias = var.kms_key_alias
  rules = [
    {
      name                     = var.name-backup
      schedule                 = var.schedule 
      start_window             = var.start_window
      completion_window        = var.completion_window
      enable_continuous_backup = var.enable_continuous_backup
      lifecycle                = var.backup_lifecycle
      copy_action              = var.copy_action
      recovery_point_tags      = var.recovery_point_tags
    }

  ]
  selections = [
        {
      name      = var.name-backup
      resources = [module.db_instance.this_db_instance_arn]
    },
  ]
  tags = var.tags

}

resource "aws_cloudwatch_log_group" "this" {
  for_each          = toset(var.enabled_cloudwatch_logs_exports)
  name              = var.master_db_identifier == "" ? "/aws/rds/instance/${var.identifier}-master/${each.key}" : "/aws/rds/instance/${var.master_db_identifier}/${each.key}" 
  retention_in_days = var.log_retention_in_days
  kms_key_id        = var.encrypt_cloudwatch_log_group ? data.aws_kms_key.by_alias[0].arn : null
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", "/aws/rds/instance/${var.identifier}-master/${each.key}")
    },
  )
}

resource "aws_cloudwatch_log_group" "rds_os_metrics" {
  count             = var.create_rds_os_metrics_log_group ? 1 : 0
  name              = "RDSOSMetrics"
  retention_in_days = var.log_retention_in_days_os_metrics == "" ? var.log_retention_in_days : var.log_retention_in_days_os_metrics
  kms_key_id        = var.encrypt_cloudwatch_log_group ? data.aws_kms_key.by_alias[0].arn : null
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", "RDSOSMetrics")
    },
  )
}

resource "aws_cloudwatch_log_group" "replica_audit" {
  count             = var.create_read_replica && var.amount_read_replica > 0 && contains(var.enabled_cloudwatch_logs_exports, "audit") ? var.amount_read_replica : 0
  name              = var.replica_identifier == "" ? "/aws/rds/instance/${var.identifier}-replica-${count.index}/audit" : "/aws/rds/instance/${var.replica_identifier}/audit"
  retention_in_days = var.log_retention_in_days
  kms_key_id        = var.encrypt_cloudwatch_log_group ? data.aws_kms_key.by_alias[0].arn : null
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", "/aws/rds/instance/${var.identifier}-replica-${count.index}/audit")
    },
  )
}

resource "aws_cloudwatch_log_group" "replica_error" {
  count             = var.create_read_replica && var.amount_read_replica > 0 && contains(var.enabled_cloudwatch_logs_exports, "error") ? var.amount_read_replica : 0
  name              = var.replica_identifier == "" ? "/aws/rds/instance/${var.identifier}-replica-${count.index}/error" : "/aws/rds/instance/${var.replica_identifier}/error"
  retention_in_days = var.log_retention_in_days
  kms_key_id        = var.encrypt_cloudwatch_log_group ? data.aws_kms_key.by_alias[0].arn : null
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", "/aws/rds/instance/${var.identifier}-replica-${count.index}/error")
    },
  )
}

resource "aws_cloudwatch_log_group" "replica_general" {
  count             = var.create_read_replica && var.amount_read_replica > 0 && contains(var.enabled_cloudwatch_logs_exports, "general") ? var.amount_read_replica : 0
  name              = var.replica_identifier == "" ? "/aws/rds/instance/${var.identifier}-replica-${count.index}/general" : "/aws/rds/instance/${var.replica_identifier}/general"
  retention_in_days = var.log_retention_in_days
  kms_key_id        = var.encrypt_cloudwatch_log_group ? data.aws_kms_key.by_alias[0].arn : null
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", "/aws/rds/instance/${var.identifier}-replica-${count.index}/general")
    },
  )
}

resource "aws_cloudwatch_log_group" "replica_slowquery" {
  count             = var.create_read_replica && var.amount_read_replica > 0 && contains(var.enabled_cloudwatch_logs_exports, "slowquery") ? var.amount_read_replica : 0
  name              = var.replica_identifier == "" ? "/aws/rds/instance/${var.identifier}-replica-${count.index}/slowquery" : "/aws/rds/instance/${var.replica_identifier}/slowquery"
  retention_in_days = var.log_retention_in_days
  kms_key_id        = var.encrypt_cloudwatch_log_group ? data.aws_kms_key.by_alias[0].arn : null
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", "/aws/rds/instance/${var.identifier}-replica-${count.index}/slowquery")
    },
  )
}
