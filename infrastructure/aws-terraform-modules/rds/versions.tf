terraform {
  required_version = ">= 0.12.26"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.49"
    }
  
    mysql = {
      source  = "terraform-providers/mysql"
      version = ">= 1.5"
    }
    
  }
}
