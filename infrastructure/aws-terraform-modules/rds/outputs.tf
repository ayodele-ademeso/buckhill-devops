output "rds-master-db-instance-id" {
  description = "The Master DB instance ID"
  value       = module.db_instance.this_db_instance_id
}

output "rds_db_instance_endpoint" {
  description = "The connection endpoint"
  value       = module.db_instance.this_db_instance_endpoint
}

