output "iam_users_created" {
  value       = module.iam_users[*].iam_user_name
  description = "IAM users created"
}

output "iam_groups_created" {
  value       = module.iam_groups[*].group_name
  description = "IAM groups created"
}

output "credentials_files_generated" {
  value       = module.iam_users[*].iam_credentials_file_generated
  description = "local files containing iam users credentials"
}




