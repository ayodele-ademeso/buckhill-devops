locals {
  user_name = element(concat(aws_iam_user.this.*.name, [var.name]), 0)
}

resource "aws_iam_user" "this" {
  count = var.create_user ? 1 : 0

  name                 = var.name
  path                 = var.path
  force_destroy        = var.force_destroy
  permissions_boundary = var.permissions_boundary

  tags = merge(
    {
      Name = var.name
    },
    var.tags,
  )
}

resource "aws_iam_user_login_profile" "this" {
  count = var.create_user && var.create_iam_user_login_profile ? 1 : 0

  user                    = aws_iam_user.this[0].name
  pgp_key                 = var.pgp_key
  password_length         = var.password_length
  password_reset_required = var.password_reset_required
}

resource "aws_iam_access_key" "this" {
  count = var.create_user && var.create_iam_access_key && var.pgp_key != "" ? 1 : 0

  user    = aws_iam_user.this[0].name
  pgp_key = var.pgp_key
}

resource "aws_iam_access_key" "this_no_pgp" {
  count = var.create_user && var.create_iam_access_key && var.pgp_key == "" ? 1 : 0

  user = aws_iam_user.this[0].name
}

resource "aws_iam_user_ssh_key" "this" {
  count = var.create_user && var.upload_iam_user_ssh_key ? 1 : 0

  username   = aws_iam_user.this[0].name
  encoding   = var.ssh_key_encoding
  public_key = var.ssh_public_key
}

resource "aws_iam_policy" "this" {
  count = length(var.custom_user_policies)
  
  name        = lookup(var.custom_user_policies[count.index], "name", count.index)
  description = lookup(var.custom_user_policies[count.index], "description", "")
  name_prefix = lookup(var.custom_user_policies[count.index], "name_prefix", null)
  path        = lookup(var.custom_user_policies[count.index], "path", "/")
  policy      = lookup(var.custom_user_policies[count.index], "policy", null)
  tags        = lookup(var.custom_user_policies[count.index], "tags", {})
}

resource "aws_iam_user_policy" "this" {
  count = length(var.user_custom_policies)
  
  name        = lookup(var.user_custom_policies[count.index], "name", count.index)
  name_prefix = lookup(var.user_custom_policies[count.index], "name_prefix", null)
  policy      = lookup(var.user_custom_policies[count.index], "policy", null)
  user        = aws_iam_user.this[0].name
}


resource "aws_iam_user_policy_attachment" "this" {
  count = length(var.custom_user_policies)

  user      = local.user_name
  policy_arn = element(aws_iam_policy.this.*.arn, count.index)
}

resource "aws_iam_user_policy_attachment" "custom_arns" {
  count = length(var.custom_user_policy_arns)

  user      = local.user_name
  policy_arn = element(var.custom_user_policy_arns, count.index)
}


## output file ##

data "aws_caller_identity" "current" {}

resource "local_file" "iam_user_credentials" {
  count = var.create_credentials_local_file ? 1 : 0
  content              = local.user
  filename             = pathexpand("${var.credentials_file_output_path}${aws_iam_user.this[0].name}")
  file_permission      = "0644"
  directory_permission = "0755"
  depends_on           = [aws_iam_user.this]
}

locals {
  secret_access_key = aws_iam_access_key.this_no_pgp[0].secret == null ? "N/A" : aws_iam_access_key.this_no_pgp[0].secret
  smtp_secret_access_key = aws_iam_access_key.this_no_pgp[0].ses_smtp_password_v4 == null ? "N/A" : aws_iam_access_key.this_no_pgp[0].ses_smtp_password_v4
}

locals {
  user = templatefile("${path.module}/iam_user_template.tpl", {
    username       = aws_iam_user.this[0].name
    login_password = var.create_iam_user_login_profile ? aws_iam_user_login_profile.this[0].encrypted_password : "N/A"
    access_key_id  = var.pgp_key != "" ? aws_iam_access_key.this[0].id : aws_iam_access_key.this_no_pgp[0].id
    #secret_access_key          = var.create_smtp_user ? "N/A" : aws_iam_access_key.this[0].encrypted_secret
    secret_access_key      = var.create_smtp_user ? "N/A" : local.secret_access_key
    smtp_secret_access_key = var.create_smtp_user ? local.smtp_secret_access_key : "N/A"
    account_id             = data.aws_caller_identity.current.account_id
  })
}


