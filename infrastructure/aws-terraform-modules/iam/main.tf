terraform {
  backend "s3" {}
}

############
# IAM users
############
module "iam_users" {
  source = "./modules/iam-user"
  count  = length(var.users)

  name                          = lookup(var.users[count.index], "name", count.index)
  create_iam_user_login_profile = lookup(var.users[count.index], "create_iam_user_login_profile", true)
  create_iam_access_key         = lookup(var.users[count.index], "create_iam_access_key", true)
  path                          = lookup(var.users[count.index], "path", "/")
  force_destroy                 = lookup(var.users[count.index], "force_destroy", false)
  pgp_key                       = lookup(var.users[count.index], "pgp_key", "")
  password_reset_required       = lookup(var.users[count.index], "password_reset_required", true)
  password_length               = lookup(var.users[count.index], "password_length", 20)
  upload_iam_user_ssh_key       = lookup(var.users[count.index], "upload_iam_user_ssh_key", false)
  ssh_key_encoding              = lookup(var.users[count.index], "ssh_key_encoding", "SSH")
  ssh_public_key                = lookup(var.users[count.index], "ssh_public_key", "")
  custom_user_policies          = lookup(var.users[count.index], "custom_user_policies", [])
  user_custom_policies          = lookup(var.users[count.index], "user_custom_policies", [])
  custom_user_policy_arns       = lookup(var.users[count.index], "custom_user_policy_arns", [])
  create_smtp_user              = lookup(var.users[count.index], "create_smtp_user", false)
  create_credentials_local_file = lookup(var.users[count.index], "create_credentials_local_file", true)
  permissions_boundary          = lookup(var.users[count.index], "permissions_boundary", "")
  credentials_file_output_path  = lookup(var.users[count.index], "credentials_file_output_path", "~/.credentials/")
  tags                          = merge(var.tags,lookup(var.users[count.index], "tags", {} ))
}
############
# IAM groups
############
module "iam_groups" {
  source = "./modules/iam-group-with-policies"
  count  = length(var.groups)

  name                                     = lookup(var.groups[count.index], "name", count.index)
  group_users                              = lookup(var.groups[count.index], "group_users", [])
  custom_group_policy_arns                 = lookup(var.groups[count.index], "custom_group_policy_arns", [])
  custom_group_policies                    = lookup(var.groups[count.index], "custom_group_policies", [])
  attach_iam_self_management_policy        = lookup(var.groups[count.index], "attach_iam_self_management_policy", false)
  iam_self_management_policy_name_prefix   = lookup(var.groups[count.index], "iam_self_management_policy_name_prefix", "IAMSelfManagement-")
  aws_account_id                           = lookup(var.groups[count.index], "aws_account_id", "")
  tags                                     = merge(var.tags,lookup(var.groups[count.index], "tags", {} ))
  depends_on = [module.iam_users]
}

############
# IAM account
############
module "iam_account" {
  count = var.enable_iam_account_policy ? 1 : 0
  source = "./modules/iam-account"

  manage_account_alias                     = var.manage_account_alias
  account_alias                            = var.account_alias
  create_account_password_policy           = var.create_account_password_policy
  max_password_age                         = var.max_password_age
  minimum_password_length                  = var.minimum_password_length
  allow_users_to_change_password           = var.allow_users_to_change_password
  hard_expiry                              = var.hard_expiry
  password_reuse_prevention                = var.password_reuse_prevention
  require_lowercase_characters             = var.require_lowercase_characters
  require_uppercase_characters             = var.require_uppercase_characters
  require_numbers                          = var.require_numbers
  require_symbols                          = var.require_symbols
}

resource "aws_accessanalyzer_analyzer" "example" {
  count = var.enable_iam_accessanalyzer ? 1 : 0

  analyzer_name = var.iam_accessanalyzer_name
  type          = var.iam_accessanalyzer_type 
  tags          = merge (
    var.tags,
    {
      "Name" = var.iam_accessanalyzer_name
    },
  )
}