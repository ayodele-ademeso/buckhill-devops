output "eks-cluster-endpoint" {
  description = "The endpoint for your EKS Kubernetes API."
  value       = element(concat(aws_eks_cluster.this.*.endpoint, [""]), 0)
}

output "worker_nodes_ec2_ids" {
    value =  data.aws_instances.worker_nodes.*.ids
}