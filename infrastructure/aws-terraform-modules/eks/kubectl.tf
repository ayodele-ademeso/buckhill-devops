resource "local_file" "kubeconfig" {
  count                = var.write_kubeconfig && var.create_eks ? 1 : 0
  content              = local.kubeconfig
  #filename             = substr(var.config_output_path, -1, 1) == "/" ? "${var.config_output_path}config" : var.config_output_path
  filename             = pathexpand("${var.config_output_path}${var.cluster_name}")
  file_permission      = "0644"
  directory_permission = "0755"
}
