apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${cluster_auth_base64}
    server: ${endpoint}
  name: arn:aws:eks:${region}:${account_id}:cluster/${cluster_name}
contexts:
- context:
    cluster: arn:aws:eks:${region}:${account_id}:cluster/${cluster_name}
    user: arn:aws:eks:${region}:${account_id}:cluster/${cluster_name}
  name: arn:aws:eks:${region}:${account_id}:cluster/${cluster_name}
current-context: arn:aws:eks:${region}:${account_id}:cluster/${cluster_name}
kind: Config
preferences: {}
users:
- name: arn:aws:eks:${region}:${account_id}:cluster/${cluster_name}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
%{~ for i in aws_authenticator_command_args }
      - ${i}
%{~ endfor ~}
%{ for i in aws_authenticator_additional_args }
      - ${i}
%{~ endfor ~}
%{ if length(aws_authenticator_env_variables) > 0 }
      env:
  %{~ for k, v in aws_authenticator_env_variables ~}
      - name: ${k}
          value: ${v}
  %{~ endfor ~}
%{ endif }
      command: ${aws_authenticator_command}
