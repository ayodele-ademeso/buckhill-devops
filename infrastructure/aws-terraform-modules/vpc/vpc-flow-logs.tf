locals {
  # Only create flow log if user selected to create a VPC as well
  enable_flow_log = var.create_vpc && var.enable_flow_log

  create_flow_log_cloudwatch_iam_role  = local.enable_flow_log && var.flow_log_destination_type != "s3" && var.create_flow_log_cloudwatch_iam_role
  create_flow_log_cloudwatch_log_group = local.enable_flow_log && var.flow_log_destination_type != "s3" && var.create_flow_log_cloudwatch_log_group

  flow_log_destination_arn = local.create_flow_log_cloudwatch_log_group ? aws_cloudwatch_log_group.flow_log[0].arn : var.flow_log_destination_arn
  #flow_log_iam_role_arn    = var.flow_log_destination_type != "s3" && local.create_flow_log_cloudwatch_iam_role ? aws_iam_role.vpc_flow_log_cloudwatch[0].arn : var.flow_log_cloudwatch_iam_role_arn
  flow_log_iam_role_arn    = aws_iam_role.vpc_flow_log_cloudwatch.arn
}

###################
# Flow Log
###################
resource "aws_flow_log" "this" {
  count = local.enable_flow_log && local.create_flow_log_cloudwatch_log_group ? 1 : 0

  log_destination_type     = var.flow_log_destination_type
  log_destination          = local.flow_log_destination_arn
  log_format               = var.flow_log_log_format
  iam_role_arn             = local.flow_log_iam_role_arn
  traffic_type             = var.flow_log_traffic_type
  vpc_id                   = local.vpc_id
  max_aggregation_interval = var.flow_log_max_aggregation_interval

  tags = merge(var.tags, var.vpc_flow_log_tags)
}

resource "aws_flow_log" "s3_bucket" {
  count = local.enable_flow_log  && var.flow_log_destination_type == "s3" ? 1 : 0

  log_destination_type     = var.flow_log_destination_type
  log_destination          = aws_s3_bucket.log_bucket[0].arn
  log_format               = var.flow_log_log_format
  traffic_type             = var.flow_log_traffic_type
  vpc_id                   = local.vpc_id
  max_aggregation_interval = var.flow_log_max_aggregation_interval

  tags = merge(var.tags, var.vpc_flow_log_tags)
}


#####################
# Flow Log CloudWatch
#####################
resource "aws_cloudwatch_log_group" "flow_log" {
  count = local.create_flow_log_cloudwatch_log_group ? 1 : 0

  name              = var.flow_log_main_vpc_cw_log_group_name == "" ? "${var.flow_log_cloudwatch_log_group_name_prefix}${local.vpc_id}" : var.flow_log_main_vpc_cw_log_group_name
  retention_in_days = var.flow_log_cloudwatch_log_group_retention_in_days
  kms_key_id        = var.encrypt_cw_log_group ? data.aws_kms_key.by_alias.arn : null

  tags = merge(var.tags, var.vpc_flow_log_tags)
}

########################
# Flow Log CloudWatch IAM
#########################
resource "aws_iam_role" "vpc_flow_log_cloudwatch" {
  #count = local.create_flow_log_cloudwatch_iam_role ? 1 : 0

  #name_prefix          = "vpc-flow-log-role-"
  name                 = var.flow_log_cloudwatch_iam_role_name
  assume_role_policy   = data.aws_iam_policy_document.flow_log_cloudwatch_assume_role.json#[0].json
  permissions_boundary = var.vpc_flow_log_permissions_boundary

    tags = merge(
    {
      "Name" = var.flow_log_cloudwatch_iam_role_name 
    },
    var.tags,
    var.vpc_flow_log_tags,
  )
  
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.vpc_flow_log_cloudwatch.name
  policy_arn = "arn:aws:iam::aws:policy/AWSSupportAccess"
}

data "aws_iam_policy_document" "flow_log_cloudwatch_assume_role" {
 # count = local.create_flow_log_cloudwatch_iam_role  ? 1 : 0

  statement {
    principals {
      type        = "Service"
      identifiers = ["vpc-flow-logs.amazonaws.com"]
    }

    effect = "Allow"

    actions = ["sts:AssumeRole"]
  }
}



resource "aws_iam_role_policy_attachment" "vpc_flow_log_cloudwatch" {
  count = local.create_flow_log_cloudwatch_iam_role && ( var.use_inline_policy_cw_iam_role == false ) ? 1 : 0

  role       = aws_iam_role.vpc_flow_log_cloudwatch.name
  policy_arn = aws_iam_policy.vpc_flow_log_cloudwatch[0].arn
}

resource "aws_iam_policy" "vpc_flow_log_cloudwatch" {
  count = local.create_flow_log_cloudwatch_iam_role && ( var.use_inline_policy_cw_iam_role == false ) ? 1 : 0

  name_prefix = "vpc-flow-log-to-cloudwatch-"
  policy      = data.aws_iam_policy_document.vpc_flow_log_cloudwatch[0].json
}

resource "aws_iam_role_policy" "vpc_flow_log_cloudwatch_inline_policy" {
  count = local.create_flow_log_cloudwatch_iam_role && var.use_inline_policy_cw_iam_role ? 1 : 0

  name = var.inline_policy_name
  role = aws_iam_role.vpc_flow_log_cloudwatch.name
  policy      = var.inline_policy == "" ? data.aws_iam_policy_document.vpc_flow_log_cloudwatch[0].json : var.inline_policy
}

resource "aws_iam_policy" "vpc_flow_log_s3" {
  count = ( local.enable_flow_log || var.enable_flow_log_default_vpc )  && var.flow_log_destination_type == "s3" ? 1 : 0

  name_prefix = "vpc-flow-log-to-cloudwatch-"
  policy      = data.aws_iam_policy_document.vpc_flow_log_s3[0].json
}

data "aws_iam_policy_document" "vpc_flow_log_cloudwatch" {
  count =( local.enable_flow_log || var.enable_flow_log_default_vpc )  && local.create_flow_log_cloudwatch_iam_role ? 1 : 0

  statement {
    sid = "AWSVPCFlowLogsPushToCloudWatch"

    effect = "Allow"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "vpc_flow_log_s3" {
  count = ( local.enable_flow_log || var.enable_flow_log_default_vpc )  && var.flow_log_destination_type == "s3" ? 1 : 0

  statement {
    sid = "AWSVPCFlowLogsPushToS3"

    effect = "Allow"

    actions = [
        "logs:CreateLogDelivery",
        "logs:DeleteLogDelivery",
    ]

    resources = ["*"]
  }
}

resource "aws_s3_bucket" "log_bucket" {
  count = local.enable_flow_log  && var.flow_log_destination_type == "s3"? 1 : 0
  bucket = "${var.name}-vpc-flow-log-${local.vpc_id}"
  force_destroy = true  
  acl    = "log-delivery-write"
  tags = merge(var.tags, 
  
  {
    Name = "${var.name}-vpc-flow-log-${local.vpc_id}"
  })
} 



resource "aws_s3_bucket_policy" "b" {
  count = local.enable_flow_log   && var.flow_log_destination_type == "s3" ? 1 : 0
  bucket = aws_s3_bucket.log_bucket[0].id 
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "MYBUCKETPOLICY"
    Statement = [
      {
        Sid       = "Logging"
        Effect    = "Allow"
        
        Principal =  {
          Service = "delivery.logs.amazonaws.com"
        }

  
        Action    = ["s3:GetBucketAcl", 
      "s3:ListBucket",
      "s3:PutObject"]
        Resource = ["arn:aws:s3:::${var.name}-vpc-flow-log-${local.vpc_id}/*","arn:aws:s3:::${var.name}-vpc-flow-log-${local.vpc_id}"
        ]

      },
    ]
  })
} 

resource "aws_s3_bucket_policy" "b2" {
  count = var.enable_flow_log_default_vpc  && var.flow_log_destination_type == "s3" ? 1 : 0
  bucket = aws_s3_bucket.log_bucket2[0].id 
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "MYBUCKETPOLICY"
    Statement = [
      {
        Sid       = "Logging"
        Effect    = "Allow"
        
        Principal =  {
          Service = "delivery.logs.amazonaws.com"
        }

  
        Action    = ["s3:GetBucketAcl", 
      "s3:ListBucket",
      "s3:PutObject"]
        Resource = ["arn:aws:s3:::${var.name}-vpc-flow-log-${data.aws_vpc.default.id}/*","arn:aws:s3:::${var.name}-vpc-flow-log-${data.aws_vpc.default.id}"
        ]

      },
    ]
  })
} 
