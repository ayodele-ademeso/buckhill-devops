
output "vpc-name" {
  description = "The name of the VPC specified as argument to this module"
  value       = var.name
}

output "vpc-id" {
  description = "The ID of the VPC "
  value       = aws_vpc.this[0].id 
}

output "private-subnets-ids" {
  description = "List of IDs of private subnets"
  value       = aws_subnet.private.*.id
}

output "public-subnets-ids" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public.*.id
}

