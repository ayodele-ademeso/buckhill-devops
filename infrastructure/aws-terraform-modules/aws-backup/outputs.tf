
output "Please-check-your-mailbox-and-confirm-the-subscription-for-aws-backup-events" {
  description = "SNS topic subscriptions"
  value       =  var.module_enabled ? concat(aws_sns_topic_subscription.sns_subscription.*.endpoint, [""]) : null
}

