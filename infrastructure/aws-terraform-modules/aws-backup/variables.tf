
variable "name" {
  description = "The display name of a backup plan and vault to be created"
  type        = string
}

variable "kms_key_arn" {
  description = "The server-side encryption key that is used to protect your backups"
  type        = string
  default     = null
}

variable "kms_key_alias" {
  description = "the alias of the kms key to encrypt SNS topic"
  type        = string
  default     = ""
}

variable "notification" {
  description = "list of Backup Vault Events to publish for sns notification "
  type        = list(string)
  default     = []
}

variable "module_enabled" {
  type        = bool
  default     = true
  description = "Whether to create this module"
}


variable "enable_sns" {
  type        = bool
  default     = true
  description = "Whether to enable SNS resources"
}

variable "selections" {
  description = "A list of selection maps"
  type        = any
  default     = []
}

variable "rules" {
  description = "A list of rule maps"
  type        = any
  default     = []
}


variable "emails" {
  description = "list of emails to subscribe to SNS for AWS backup event notifications"
  type        = list(string)
  default     = []
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags"
}