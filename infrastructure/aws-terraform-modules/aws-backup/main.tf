terraform {
  backend "s3" {}
}

data "aws_kms_key" "by_alias" {
  key_id = "alias/${var.kms_key_alias}"
}

resource "aws_sns_topic" "sns_topic" {
  count = var.module_enabled && var.enable_sns ? 1 : 0 
  name = "${var.name}-backup-vault-events"
  display_name = "AWS Bakcup vault events"
  content_based_deduplication = false
  kms_master_key_id = data.aws_kms_key.by_alias.id
  fifo_topic = false
  tags = merge(
    var.tags,
    {
      "Name" = format("%s","${var.name}-backup-vault-events" )
    },
  )
}



resource "aws_sns_topic_subscription" "sns_subscription" {
  count = var.module_enabled && var.enable_sns ? length(var.emails) : 0 
  endpoint = element(var.emails, count.index)
  protocol = "email" 
  topic_arn = aws_sns_topic.sns_topic[0].arn
  depends_on = [
    aws_sns_topic.sns_topic,
  ]

}

module "backup_sns" {
  count = var.module_enabled && var.enable_sns ? 1 : 0 
  source = "./modules/backup"

  name          = var.name
  notifications = {
    sns_topic_arn       = aws_sns_topic.sns_topic[0].arn
    backup_vault_events = var.notification 
  }
  rules         = var.rules
  selections    = var.selections
  tags          = var.tags
}

module "backup" {
  count = var.module_enabled && (var.enable_sns == false) ? 1 : 0 
  source = "./modules/backup"

  name          = var.name
  rules         = var.rules
  selections    = var.selections
  tags          = var.tags
}

