output "efs-dns-name" {
  value       = local.dns_name
  description = "EFS DNS name"
}
