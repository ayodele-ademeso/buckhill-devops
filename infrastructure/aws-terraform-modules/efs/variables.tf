variable "name-vpc" {
  description = "Name of VPC"
  type        = string
  default     = ""
}

variable "name-backup" {
  description = "Name to be used for AWS bakcup"
  type        = string
  default     = ""
}

variable "efs_security_group_name" {
  description = "Name to be used for EFS security group"
  type        = string
  default     = ""
}

variable "efs_security_group_description" {
  description = "Description to be used for EFS security group"
  type        = string
  default     = ""
}

variable "efs_automatic_aws_backup" {
  description = "Enable/Disable EFS automatic AWS Backup "
  type        = bool
  default     = false
}

variable "emails-efs-backup" {
  description = "list of emails to subscribe to EFS backup notification events"
  type        = list(string)
  default     = []
}

variable "subnet_ids" {
  description = "list of subnet IDs where to implement EFS moount targets"
  type        = list(string)
  default     = []
}

variable "enable_sns" {
  type        = bool
  default     = false
  description = "Whether to enable SNS resources"
}

variable "use_private_subnets" {
  type        = bool
  description = "Wether mount targets are for private subnets or not (public subnets)"
  default     = true
}

variable "enable_aws_backup" {
  description = "Wether to enable AWS Backup"
  type        = bool
  default     = false
}

variable "notification-backup" {
  description = "list of aws backup notifications"
  type        = list(string)
  default     = []
}


variable "custom_efs_sg_ingress_rules" {
  description = "Custom EFS ingress rules"
  type        = any
  default     = []
}

variable "security_groups" {
  type        = list(string)
  description = "Security group IDs to allow access to the EFS"
  default     = []
}

variable "allowed_cidr_blocks" {
  type        = list(string)
  default     = []
  description = "The CIDR blocks from which to allow `ingress` traffic to the EFS"
}

variable "access_points" {
  type        = map(map(map(any)))
  default     = {}
  description = "A map of the access points you would like in your EFS volume"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID"
  default     = ""
}

variable "region" {
  type        = string
  description = "AWS Region"
}

variable "subnets" {
  type        = list(string)
  description = "Subnet IDs"
  default     = []
}

variable "zone_id" {
  type        = string
  description = "Route53 DNS zone ID"
  default     = ""
}

variable "encrypted" {
  type        = bool
  description = "If true, the file system will be encrypted"
  default     = true
}

variable "kms_key_id" {
  type        = string
  description = "If set, use a specific KMS key"
  default     = null
}

variable "kms_key_alias" {
  description = "the alias of the kms key to encrypt SNS topic for aws config"
  type        = string
  default     = ""
}

variable "performance_mode" {
  type        = string
  description = "The file system performance mode. Can be either `generalPurpose` or `maxIO`"
  default     = "generalPurpose"
}

variable "provisioned_throughput_in_mibps" {
  default     = 0
  description = "The throughput, measured in MiB/s, that you want to provision for the file system. Only applicable with `throughput_mode` set to provisioned"
}

variable "throughput_mode" {
  type        = string
  description = "Throughput mode for the file system. Defaults to bursting. Valid values: `bursting`, `provisioned`. When using `provisioned`, also set `provisioned_throughput_in_mibps`"
  default     = "bursting"
}

variable "mount_target_ip_address" {
  type        = string
  description = "The address (within the address range of the specified subnet) at which the file system may be mounted via the mount target"
  default     = null
}

variable "dns_name" {
  type        = string
  description = "Name of the CNAME record to create"
  default     = ""
}

variable "transition_to_ia" {
  type        = string
  description = "Indicates how long it takes to transition files to the IA storage class. Valid values: AFTER_7_DAYS, AFTER_14_DAYS, AFTER_30_DAYS, AFTER_60_DAYS and AFTER_90_DAYS"
  default     = ""
}


# Copy contents of cloudposse/terraform-null-label/variables.tf here

variable "context" {
  type = any
  default = {
    enabled             = true
    namespace           = null
    environment         = null
    stage               = null
    name                = null
    delimiter           = null
    attributes          = []
    tags                = {}
    additional_tag_map  = {}
    regex_replace_chars = null
    label_order         = []
    id_length_limit     = null
    label_key_case      = null
    label_value_case    = null
  }
  description = <<-EOT
    Single object for setting entire context at once.
    See description of individual variables for details.
    Leave string and numeric variables as `null` to use default value.
    Individual variable settings (non-null) override settings in context object,
    except for attributes, tags, and additional_tag_map, which are merged.
  EOT

  validation {
    condition     = lookup(var.context, "label_key_case", null) == null ? true : contains(["lower", "title", "upper"], var.context["label_key_case"])
    error_message = "Allowed values: `lower`, `title`, `upper`."
  }

  validation {
    condition     = lookup(var.context, "label_value_case", null) == null ? true : contains(["lower", "title", "upper", "none"], var.context["label_value_case"])
    error_message = "Allowed values: `lower`, `title`, `upper`, `none`."
  }
}

variable "enabled" {
  type        = bool
  default     = true
  description = "Set to false to prevent the module from creating any resources"
}

variable "namespace" {
  type        = string
  default     = null
  description = "Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp'"
}

variable "environment" {
  type        = string
  default     = null
  description = "Environment, e.g. 'uw2', 'us-west-2', OR 'prod', 'staging', 'dev', 'UAT'"
}

variable "stage" {
  type        = string
  default     = null
  description = "Stage, e.g. 'prod', 'staging', 'dev', OR 'source', 'build', 'test', 'deploy', 'release'"
}

variable "name" {
  type        = string
  default     = null
  description = "Solution name, e.g. 'app' or 'jenkins'"
}

variable "delimiter" {
  type        = string
  default     = null
  description = <<-EOT
    Delimiter to be used between `namespace`, `environment`, `stage`, `name` and `attributes`.
    Defaults to `-` (hyphen). Set to `""` to use no delimiter at all.
  EOT
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit','XYZ')`"
}

variable "efs_sg_tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags for efs security group"
}

variable "additional_tag_map" {
  type        = map(string)
  default     = {}
  description = "Additional tags for appending to tags_as_list_of_maps. Not added to `tags`."
}

variable "label_order" {
  type        = list(string)
  default     = null
  description = <<-EOT
    The naming order of the id output and Name tag.
    Defaults to ["namespace", "environment", "stage", "name", "attributes"].
    You can omit any of the 5 elements, but at least one must be present.
  EOT
}

variable "regex_replace_chars" {
  type        = string
  default     = null
  description = <<-EOT
    Regex to replace chars with empty string in `namespace`, `environment`, `stage` and `name`.
    If not set, `"/[^a-zA-Z0-9-]/"` is used to remove all characters other than hyphens, letters and digits.
  EOT
}

variable "id_length_limit" {
  type        = number
  default     = null
  description = <<-EOT
    Limit `id` to this many characters (minimum 6).
    Set to `0` for unlimited length.
    Set to `null` for default, which is `0`.
    Does not affect `id_full`.
  EOT
  validation {
    condition     = var.id_length_limit == null ? true : var.id_length_limit >= 6 || var.id_length_limit == 0
    error_message = "The id_length_limit must be >= 6 if supplied (not null), or 0 for unlimited length."
  }
}

variable "label_key_case" {
  type        = string
  default     = null
  description = <<-EOT
    The letter case of label keys (`tag` names) (i.e. `name`, `namespace`, `environment`, `stage`, `attributes`) to use in `tags`.
    Possible values: `lower`, `title`, `upper`.
    Default value: `title`.
  EOT

  validation {
    condition     = var.label_key_case == null ? true : contains(["lower", "title", "upper"], var.label_key_case)
    error_message = "Allowed values: `lower`, `title`, `upper`."
  }
}

variable "label_value_case" {
  type        = string
  default     = null
  description = <<-EOT
    The letter case of output label values (also used in `tags` and `id`).
    Possible values: `lower`, `title`, `upper` and `none` (no transformation).
    Default value: `lower`.
  EOT

  validation {
    condition     = var.label_value_case == null ? true : contains(["lower", "title", "upper", "none"], var.label_value_case)
    error_message = "Allowed values: `lower`, `title`, `upper`, `none`."
  }
}
#### End of copy of cloudposse/terraform-null-label/variables.tf



variable "schedule" {
  description = "schedule cron expression for aws backup"
  type        = string
  default     = ""
}

variable "start_window" {
  description = "start_window for aws backup"
  type        = number
  default     = 10
}

variable "completion_window" {
  description = "completion_window for aws backup"
  type        = number
  default     = 100
}

variable "enable_continuous_backup" {
  description = "wether to enable_continuous_backup for aws backup"
  type        = bool
  default     = false
}

variable "backup_lifecycle" {
  description = "lifecycle for aws backup"
  type        = map(string)
  default     = {}
}

variable "copy_action" {
  description = "copy_action for aws backup"
  type        = map(string)
  default     = {}
}

variable "recovery_point_tags" {
  description = "recovery_point_tags for aws backup"
  type        = map(string)
  default     = {}
}
