terraform {
  backend "s3" {}
}

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = [var.name-vpc]
  }
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.main.id
}

data "aws_subnet_ids" "public" {
  count = var.use_private_subnets ? 0 : 1
  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = ["${var.name-vpc}-pub*"]
  }

}

data "aws_subnet_ids" "private" {
  count = var.use_private_subnets ? 1 : 0
  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = ["${var.name-vpc}-priv*"]
  }
}

locals {
  dns_name = "${join("", aws_efs_file_system.default.*.id)}.efs.${var.region}.amazonaws.com"
}

resource "aws_efs_file_system" "default" {
  count                           = module.this.enabled ? 1 : 0
  tags                            = module.this.tags
  encrypted                       = var.encrypted
  kms_key_id                      = var.kms_key_id
  performance_mode                = var.performance_mode
  provisioned_throughput_in_mibps = var.provisioned_throughput_in_mibps
  throughput_mode                 = var.throughput_mode

  dynamic "lifecycle_policy" {
    for_each = var.transition_to_ia == "" ? [] : [1]
    content {
      transition_to_ia = var.transition_to_ia
    }
  }
}

resource "aws_efs_mount_target" "default" {
  count           = module.this.enabled ? length(local.efs_subnets) : 0
  file_system_id  = join("", aws_efs_file_system.default.*.id)
  ip_address      = var.mount_target_ip_address
  subnet_id       = local.efs_subnets[count.index]
  security_groups = [join("", aws_security_group.efs.*.id)]
}

locals {
  use_private_subnets = var.use_private_subnets ? tolist(data.aws_subnet_ids.private[0].ids) : tolist(data.aws_subnet_ids.public[0].ids)
}

locals {
  efs_subnets = length(var.subnet_ids) == 0 ? local.use_private_subnets : var.subnet_ids
}
resource "aws_efs_access_point" "default" {
  for_each = var.access_points

  file_system_id = join("", aws_efs_file_system.default.*.id)

  posix_user {
    gid = var.access_points[each.key]["posix_user"]["gid"]
    uid = var.access_points[each.key]["posix_user"]["uid"]
    # Just returning null in the lookup function gives type errors and is not omitting the parameter, this work around ensures null is returned.
    secondary_gids = lookup(lookup(var.access_points[each.key], "posix_user", {}), "secondary_gids", null) == null ? null : null
  }

  root_directory {
    path = "/${each.key}"
    creation_info {
      owner_gid   = var.access_points[each.key]["creation_info"]["gid"]
      owner_uid   = var.access_points[each.key]["creation_info"]["uid"]
      permissions = var.access_points[each.key]["creation_info"]["permissions"]
    }
  }

  tags = module.this.tags
}

resource "aws_security_group" "efs" {
  count       = module.this.enabled ? 1 : 0
  name        = var.efs_security_group_name == "" ? format("%s-efs", module.this.id) : var.efs_security_group_name
  description = var.efs_security_group_description == "" ? "EFS Security Group" : var.efs_security_group_description
  vpc_id      = data.aws_vpc.main.id

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.this.tags,
    {
      "Name" = format("%s-efs", module.this.id)
    },
    var.efs_sg_tags 
  )
}

resource "aws_security_group_rule" "ingress_security_groups" {
  count                    = module.this.enabled ? length(var.security_groups) : 0
  description              = "Allow inbound traffic from existing security groups"
  type                     = "ingress"
  from_port                = "2049" # NFS
  to_port                  = "2049"
  protocol                 = "tcp"
  source_security_group_id = var.security_groups[count.index]
  security_group_id        = join("", aws_security_group.efs.*.id)
}

resource "aws_security_group_rule" "custom_efs_sg_ingress_rules" {
  count = length(var.custom_efs_sg_ingress_rules)
  type              = "ingress"
  from_port         = lookup(var.custom_efs_sg_ingress_rules[count.index], "from_port", null)
  to_port           = lookup(var.custom_efs_sg_ingress_rules[count.index], "to_port", null)
  protocol          = lookup(var.custom_efs_sg_ingress_rules[count.index], "protocol",null)
  cidr_blocks       = lookup(var.custom_efs_sg_ingress_rules[count.index], "cidr_blocks", null)
  description       = lookup(var.custom_efs_sg_ingress_rules[count.index], "description", null)
  ipv6_cidr_blocks  = lookup(var.custom_efs_sg_ingress_rules[count.index], "ipv6_cidr_blocks",null)
  security_group_id = join("", aws_security_group.efs.*.id)
}

resource "aws_security_group_rule" "ingress_cidr_blocks" {
  count             = module.this.enabled && length(var.allowed_cidr_blocks) > 0 ? 1 : 0
  description       = "Allow inbound traffic from CIDR blocks"
  type              = "ingress"
  from_port         = "2049" # NFS
  to_port           = "2049"
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr_blocks
  security_group_id = join("", aws_security_group.efs.*.id)
}

resource "aws_security_group_rule" "egress" {
  count             = module.this.enabled ? 1 : 0
  type              = "egress"
  description       = "Egress Rule"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = join("", aws_security_group.efs.*.id)
}

module "dns" {
  source  = "cloudposse/route53-cluster-hostname/aws"
  version = "0.12.0"

  enabled  = module.this.enabled && length(var.zone_id) > 0 ? true : false
  dns_name = var.dns_name == "" ? module.this.id : var.dns_name
  ttl      = 60
  zone_id  = var.zone_id
  records  = [local.dns_name]

  context = module.this.context
}

module "this" {
  source  = "cloudposse/label/null"
  version = "0.24.1" # requires Terraform >= 0.13.0

  enabled             = var.enabled
  namespace           = var.namespace
  environment         = var.environment
  stage               = var.stage
  name                = var.name
  delimiter           = var.delimiter
  attributes          = var.attributes
  tags                = var.tags
  additional_tag_map  = var.additional_tag_map
  label_order         = var.label_order
  regex_replace_chars = var.regex_replace_chars
  id_length_limit     = var.id_length_limit
  label_key_case      = var.label_key_case
  label_value_case    = var.label_value_case

  context = var.context
}

resource "aws_efs_backup_policy" "policy" {
  count = var.efs_automatic_aws_backup ? 1 : 0
  file_system_id = join("", aws_efs_file_system.default.*.id)

  backup_policy {
    status =  "ENABLED" 
  }
}

module "efs_aws_backup" {
  count = var.enable_aws_backup ? 1 : 0
  source  = "..//aws-backup"
  enable_sns = var.enable_sns
  name = var.name-backup
  notification  = var.notification-backup
  emails = var.emails-efs-backup
  kms_key_alias = var.kms_key_alias
  rules = [
    {
      name                     = var.name-backup
      schedule                 = var.schedule 
      start_window             = var.start_window
      completion_window        = var.completion_window
      enable_continuous_backup = var.enable_continuous_backup
      lifecycle                = var.backup_lifecycle
      copy_action              = var.copy_action
      recovery_point_tags      = var.recovery_point_tags
    }


  ]
  selections = [
        {
      name      = var.name-backup
      resources = [aws_efs_file_system.default[0].arn]
    },
  ]
  tags = var.tags

}